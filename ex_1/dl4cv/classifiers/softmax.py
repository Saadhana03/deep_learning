import numpy as np
from random import shuffle

def softmax_loss_naive(W, X, y, reg=0.5):
    """
    Softmax loss function, naive implementation (with loops)
  
    Inputs have dimension D, there are C classes, and we operate on minibatches
    of N examples.
  
    Inputs:
    - W: A numpy array of shape (D, C) containing weights.
    - X: A numpy array of shape (N, D) containing a minibatch of data.
    - y: A numpy array of shape (N,) containing training labels; y[i] = c means
      that X[i] has label c, where 0 <= c < C.
    - reg: (float) regularization strength
  
    Returns a tuple of:
    - loss as single float
    - gradient with respect to weights W; an array of same shape as W
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)
    D=W.shape[0]
    C=W.shape[1]
    N=X.shape[0]
    #############################################################################
    # TODO: Compute the softmax loss and its gradient using explicit loops.     #
    # Store the loss in loss and the gradient in dW. If you are not careful     #
    # here, it is easy to run into numeric instability. Don't forget the        #
    # regularization!                                                           #
    #############################################################################
    pass
    for i in range(N):
        a=np.dot(np.transpose(W),X[i])
        num_stab_const=np.exp(-a.max())
        exp_a=num_stab_const*np.exp(a)
        exp_sum_a=np.sum(exp_a)
        s=exp_a/exp_sum_a
        loss=loss-np.log(s[y[i]])

        for j in range(C):
            dW[:, j] += (s[j] - (j == y[i])) * X[i]
            #dW[:,j] += ((exp_scores[j])-(y[i]==j))*(exp_scores[i])
    loss/=N
    loss=loss+reg*np.sum(W*W)
    dW/=N

    #############################################################################
    #                          END OF YOUR CODE                                 #
    #############################################################################

    return loss, dW


def softmax_loss_vectorized(W, X, y, reg):
    """
    Softmax loss function, vectorized version.
  
    Inputs and outputs are the same as softmax_loss_naive.
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)
    D = W.shape[0]
    C = W.shape[1]
    N = X.shape[0]
    #############################################################################
    # TODO: Compute the softmax loss and its gradient using no explicit loops.  #
    # Store the loss in loss and the gradient in dW. If you are not careful     #
    # here, it is easy to run into numeric instability. Don't forget the        #
    # regularization!                                                           #
    #############################################################################
    pass
    A=X.dot(W)
    num_stab_c = A - np.max(A, axis=1).reshape(-1, 1)
    exp_A=np.exp(num_stab_c)
    exp_A_sum=np.sum(exp_A,axis=1).reshape(-1,1)
    S=exp_A/exp_A_sum
    #loss = -np.sum(np.log(S[range(N), list(y)]))
    loss = -np.sum(np.log(S[range(N), list(y)]))

    S[range(N),list(y)]-=1
    dW=np.transpose(X).dot(S)

    loss/=N
    loss=loss+reg*(np.sum(W*W))
    dW/=N
    #############################################################################
    #                          END OF YOUR CODE                                 #
    #############################################################################

    return loss, dW

