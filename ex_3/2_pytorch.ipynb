{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# PyTorch three layer Convolutional Neural Network (CNN)\n",
    "Now that you have implemented and understood all the necessary layers, we want to train a convolutional neural network. But training with our naive versions would not be feasible even for comparatively small networks. Since you already learned about losses, training procedures and solvers in the previous exercises we now want to introduce a modern deep learning framework called PyTorch (see `README.md` for installation help).\n",
    "\n",
    "These frameworks facilitate network training and prototyping by providing useful helper functions and optimized versions of the most common layers and solvers. Check out the following links to make yourself familiar with PyTorch and how to implement a small network. Keep in mind the network graph structure discussed in the lecture.\n",
    "\n",
    "http://pytorch.org/tutorials/\n",
    "\n",
    "https://github.com/pytorch/examples\n",
    "\n",
    "https://github.com/jcjohnson/pytorch-examples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import torch\n",
    "from torch.autograd import Variable\n",
    "\n",
    "from dl4cv.classifiers.cnn import ThreeLayerCNN\n",
    "from dl4cv.data_utils import get_CIFAR10_data, OverfitSampler\n",
    "from dl4cv.gradient_check import rel_error\n",
    "\n",
    "#torch.set_default_tensor_type('torch.FloatTensor')\n",
    "\n",
    "%matplotlib inline\n",
    "plt.rcParams['figure.figsize'] = (10.0, 8.0) # set default size of plots\n",
    "plt.rcParams['image.interpolation'] = 'nearest'\n",
    "plt.rcParams['image.cmap'] = 'gray'\n",
    "\n",
    "# for auto-reloading external modules\n",
    "# see http://stackoverflow.com/questions/1907993/autoreload-of-modules-in-ipython\n",
    "%load_ext autoreload\n",
    "%autoreload 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Train and validation data\n",
    "For an easier management of the train and validation data pipelines we provide you with custom `torch.utils.data.Dataset` classes. Make yourself familiar with those `Dataset` classes as well as the `DataLoader` and how you have to integrate them in your training. The `num_workers` attribute allows you to preprocess data with multiple threads.\n",
    "\n",
    "http://pytorch.org/docs/data.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the (preprocessed) CIFAR10 data. Preprocessing and dataset splitting is done\n",
    "# as in 1_softmax.ipynb from exercise 1.\n",
    "# This can take a while.\n",
    "\n",
    "train_data, val_data, test_data, mean_image = get_CIFAR10_data()\n",
    "print(\"Train size: %i\" % len(train_data))\n",
    "print(\"Val size: %i\" % len(val_data))\n",
    "print(\"Test size: %i\" % len(test_data))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "img, label = train_data[2]\n",
    "print(\"Img size: \" + str(img.size()))\n",
    "plt.figure(figsize = (10,1))\n",
    "print(\"Example image:\")\n",
    "plt.imshow(img.numpy().transpose(1,2,0) + mean_image.transpose(1,2,0))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model initialization and forward pass \n",
    "\n",
    "After you understood the core concepts of PyTorch and have a rough idea on how to implement your own model complete the initialization and forward methods of the `ThreeLayerCNN` in the `dl4cv/classifiers/cnn.py` file. Note that we do not have to implement a backward pass since this is automatically done by the framework using its dynamic graph structure.\n",
    "\n",
    "Use the cell below to check your results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dl4cv.classifiers.cnn import ThreeLayerCNN\n",
    "\n",
    "torch.manual_seed(0)\n",
    "np.random.seed(0)\n",
    "\n",
    "X = np.random.randn(2, 3, 5, 5).astype(np.float32)\n",
    "X_tensor = torch.from_numpy(X.copy())\n",
    "inputs = Variable(X_tensor)\n",
    "\n",
    "model = ThreeLayerCNN(input_dim=(3, 5, 5), num_classes=3)\n",
    "outputs = model.forward(inputs)\n",
    "correct_outputs = np.array([[-0.012747,    0.05964366,  0.03898076],\n",
    "                            [-0.01286934,  0.05963349,  0.03903975]])\n",
    "\n",
    "# The difference should be very small. We get < 1e-6\n",
    "print 'Difference between the correct and your forward pass:'\n",
    "print rel_error(correct_outputs, outputs.data.numpy())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Training and validation with the Solver\n",
    "We train and validate our previously generated model with a seperate `Solver` class defined in `dl4cv/classifiers/solver.py`. Complete the training method and try to come up with an efficient iteration scheme as well as an informative training logger.\n",
    "\n",
    "Use the cells below to test your solver. A nice trick is to train your model with just a few training samples. You should be able to overfit small datasets, which will result in very high training accuracy and comparatively low validation accuracy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dl4cv.classifiers.cnn import ThreeLayerCNN\n",
    "from dl4cv.classifiers.solver import Solver\n",
    "\n",
    "num_train = 100\n",
    "train_loader = torch.utils.data.DataLoader(train_data, batch_size=50, shuffle=False, num_workers=4,\n",
    "                                           sampler=OverfitSampler(num_train))\n",
    "val_loader = torch.utils.data.DataLoader(val_data, batch_size=50, shuffle=False, num_workers=4)\n",
    "\n",
    "overfit_model = ThreeLayerCNN()\n",
    "overfit_solver = Solver(optim_args={\"lr\": 1e-2})\n",
    "overfit_solver.train(overfit_model, train_loader, val_loader, log_nth=1, num_epochs=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plotting the loss, training accuracy, and validation accuracy should show clear overfitting:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.subplot(2, 1, 1)\n",
    "plt.plot(overfit_solver.train_loss_history, 'o')\n",
    "plt.xlabel('iteration')\n",
    "plt.ylabel('loss')\n",
    "\n",
    "plt.subplot(2, 1, 2)\n",
    "plt.plot(overfit_solver.train_acc_history, '-o')\n",
    "plt.plot(overfit_solver.val_acc_history, '-o')\n",
    "plt.legend(['train', 'val'], loc='upper left')\n",
    "plt.xlabel('epoch')\n",
    "plt.ylabel('accuracy')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Train the net\n",
    "By training the three-layer convolutional network for one epoch, you should achieve greater than 40% accuracy on the validation set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "from dl4cv.classifiers.cnn import ThreeLayerCNN\n",
    "from dl4cv.classifiers.solver import Solver\n",
    "\n",
    "train_loader = torch.utils.data.DataLoader(train_data, batch_size=50, shuffle=True, num_workers=4)\n",
    "val_loader = torch.utils.data.DataLoader(val_data, batch_size=50, shuffle=False, num_workers=4)\n",
    "\n",
    "############################################################################\n",
    "# TODO: Train your network and find the best hyperparameters               #\n",
    "############################################################################\n",
    "        \n",
    "model = ThreeLayerCNN(hidden_dim=500)\n",
    "solver = Solver(optim_args={\"lr\": 1e-3, \"weight_decay\": 0.001})\n",
    "solver.train(model, train_loader, val_loader, log_nth=100, num_epochs=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualize Filters\n",
    "You can visualize the first-layer convolutional filters from the trained network by running the following. If your kernel visualizations do not exhibit clear structures try optimizing the weight scale:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "from dl4cv.vis_utils import visualize_grid\n",
    "\n",
    "conv_params = model.conv.weight.data.numpy()\n",
    "grid = visualize_grid(conv_params.transpose(0, 2, 3, 1))\n",
    "plt.imshow(grid.astype('uint8'))\n",
    "plt.axis('off')\n",
    "plt.gcf().set_size_inches(6, 6)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Test your model\n",
    "Run your best model on the validation and test sets. You should achieve above 58% accuracy on the test set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_loader = torch.utils.data.DataLoader(test_data, batch_size=50, shuffle=False, num_workers=4)\n",
    "\n",
    "scores = []\n",
    "for batch in test_loader:\n",
    "    inputs, labels = Variable(batch[0]), Variable(batch[1])\n",
    "\n",
    "    outputs = model(inputs)\n",
    "    _, preds = torch.max(outputs, 1)\n",
    "    scores.extend((preds == labels).data.numpy())\n",
    "    \n",
    "print 'Test set accuracy: %f' % np.mean(scores)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Save the model\n",
    "\n",
    "When you are satisfied with your training, save the model for submission."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.save(\"models/three_layer_cnn.model\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Scoring function\n",
    "We will score the model you have just saved based on the classification accuracy on our test dataset. The scoring function should represent the difficulty of obtaining a good test accuracy and should therefore give 0 points for worse results than random guessing, should be linear in a first regime and exponential beyond that. The onset of exponential growth depends on the problem. In that region you get twice as many points for an additional 10% accuracy.\n",
    "\n",
    "For this problem we specifically use the following scoring function:\n",
    "\n",
    "$$f(x) = \\left\\{\n",
    "\t\\begin{array}{ll}\n",
    "\t\t0  & \\mbox{if } x \\leq 0.1 \\\\\n",
    "\t\t100x & \\mbox{if } 0.1 < x \\leq 0.58 \\\\\n",
    "        \\left(\\frac{58}{\\exp(0.58 \\ln(2)/0.1)}\\right) \\exp(x \\ln(2)/0.1) & \\mbox{if } 0.58 < x \\leq 1\n",
    "\t\\end{array}\n",
    "\\right.$$\n",
    "\n",
    "The function can be plotted in the following cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dl4cv.data_utils import scoring_function\n",
    "\n",
    "x = np.linspace(0, 1, num=1000)\n",
    "plt.plot(x, scoring_function(x, lin_exp_boundary=0.58, doubling_rate=0.1))\n",
    "plt.title('Scoring Function')\n",
    "plt.xlabel('Test Accuracy')\n",
    "plt.ylabel('Score')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
